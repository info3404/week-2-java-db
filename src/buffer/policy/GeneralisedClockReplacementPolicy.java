package buffer.policy;

import java.util.List;

import buffer.BufferFrame;

/**
 * GLOCK Replacement Policy
 */
public class GeneralisedClockReplacementPolicy implements ReplacementPolicy {

	/**
	 * maximum value you should set the clock value (i.e. no single BufferFrame 
	 * should surpass that limit value).
	 */
    private final int limit;

    /**
     * Create new GCLOCK policy object 
     * @param limit Maximum allowed value for clock value.
     */
    public GeneralisedClockReplacementPolicy(int limit) {
        this.limit = limit;
    }

    @Override
    public String getName() {
        return "GCLOCK Replacement";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) {
        assert pool.size() > 0 : "Expects a pool of at least size 1";
        // TODO: implement full solution
        return null;
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
    	// TODO: implement full solution
    }

}
