package buffer.policy;

import buffer.BufferFrame;
import disk.Page;
import disk.PageId;
import global.TestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for Medium task
 * To get these tests to pass you will need to fully implement ClockReplacementPolicy
 * (@see ClockReplacementPolicy)
 */
public class ClockReplacementPolicyTest {

    private static final int NUMFRAMES = 5;
	private ReplacementPolicy policy;
    private List<BufferFrame> pool;
    private BufferFrame A, B, C, D, E;

    @Before
    public void setUp() throws Exception {
        policy = new ClockReplacementPolicy();
        pool = TestUtils.generateBufferFrameList(NUMFRAMES);
        A = pool.get(0);
        B = pool.get(1);
        C = pool.get(2);
        D = pool.get(3);
        E = pool.get(4);
    }

    @Test
    public void testChooseWhenOneFrameUnused() throws Exception {
        TestUtils.notifyMany(policy, pool, 1, 1, 0, 1, 1);
        // Test 4/5 used, but one never used
        assertEquals("Chosen frame should be the one containing the unused page", C, policy.choose(pool));
    }

    @Test
    public void testChooseFirst() throws Exception {
        TestUtils.notifyMany(policy, pool, 1, 1, 1, 1, 1);
        // Test 4/5 used, but one never used
        assertEquals("Chosen frame should be the one containing the least-used page", B, policy.choose(pool));
    }

//    @Test
//    public void testChooseClockWithPin() throws Exception {
//        TestUtils.notifyMany(mReplacementPolicy, mPool, 1, 1, 1, 1, 1);
//        // Test 4/5 used, but one never used
//        assertEquals(A, mReplacementPolicy.choose(mPool));
//        assertEquals(B, mReplacementPolicy.choose(mPool));
//        B.pin();
//        assertEquals(C, mReplacementPolicy.choose(mPool));
//        assertEquals(D, mReplacementPolicy.choose(mPool));
//        assertEquals(E, mReplacementPolicy.choose(mPool));
//        assertEquals(A, mReplacementPolicy.choose(mPool));
//        assertEquals(C, mReplacementPolicy.choose(mPool));
//    }
//
//    @Test
//    public void testIncrementsWhenPinned() throws Exception {
//        for(BufferFrame frame : mPool) {
//            frame.pin();
//        }
//        TestUtils.notifyMany(mReplacementPolicy, mPool, 1, 1, 0, 1, 1);
//        for(BufferFrame frame : mPool) {
//            frame.unpin();
//        }
//        assertEquals(C, mReplacementPolicy.choose(mPool));
//        assertEquals(A, mReplacementPolicy.choose(mPool));
//    }

    @Test
    public void testClockLimit() throws Exception {
        TestUtils.notifyMany(policy, pool, 10, 10, 10, 10, 1);
        assertEquals("All pages have been accessed at least onee, so chosen page should just be first in pool", A, policy.choose(pool));

        TestUtils.notifyMany(policy, pool, 4, 4, 4, 4, 0);
        assertEquals("Chosen frame should be the one containing the unused page",E, policy.choose(pool));
    }

    @Test
    public void testMoveToNextFrameAfterChosen() throws Exception {
        TestUtils.notifyMany(policy, pool, 0, 0, 0, 0, 0);
        assertEquals("First chosen frame should be the first in the pool", A, policy.choose(pool));
        assertEquals("Second chosen frame should be the second in the pool", B, policy.choose(pool));
        assertEquals("Third chosen frame should be the third in the pool", C, policy.choose(pool));
    }

    @Test
    public void testChooseRecentlyReplacedFrame() throws Exception {
        TestUtils.notifyMany(policy, pool, 1, 1, 1, 1, 1);
        PageId replaceId = new PageId(10);
        Page replacePage = new Page();
        C.setPage(replaceId, replacePage);
        assertEquals("Chosen frame should be the one containing the changed page", C, policy.choose(pool));
    }

    @After
    public void testNoChangeToList() {
    	assertEquals("Pool size should not have changed",NUMFRAMES, pool.size());
    }

//    @Test(expected= BufferFrame.AllBufferFramesPinnedException.class)
//    public void testChooseAllFramesPinned() {
//        A.pin();
//        B.pin();
//        C.pin();
//        D.pin();
//        E.pin();
//        mReplacementPolicy.choose(mPool);
//    }

}