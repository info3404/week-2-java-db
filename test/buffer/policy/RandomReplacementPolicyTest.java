package buffer.policy;

import buffer.BufferFrame;
import buffer.policy.RandomReplacementPolicy;
import buffer.policy.ReplacementPolicy;
import global.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RandomReplacementPolicyTest {

    private ReplacementPolicy mReplacementPolicy;
    private List<BufferFrame> mPool;

    @Before
    public void setUp() throws Exception {
        mReplacementPolicy = new RandomReplacementPolicy();
        mPool = TestUtils.generateBufferFrameList(10);
    }

    @Test
    public void testRandomChoose() throws Exception {
        for(int i = 0; i < 20; i++) {
            BufferFrame first = mReplacementPolicy.choose(mPool);
            BufferFrame second = mReplacementPolicy.choose(mPool);
            assertNotNull("First frame should be a valid frame",first);
            assertNotNull("Second frame should be a valid frame",second);
            assertNotEquals("Pages returned from consecutive calls should be different",first, second);
        }
    }
}